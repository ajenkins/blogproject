// remap jQuery to $
(function($){})(window.jQuery);

/* trigger when page is ready */
$(document).ready(function (){
    // // Activate FancyBox for spec offers
    // $("a#open-spec").fancybox();

    // Slide FAQs open/close
    $('a.toggle-open').click(function() {
	$toggleBox = $(this).parent().siblings('.toggle-hidden');

	if(($toggleBox).is(':hidden')) {
	    $(this).addClass('open');
	} else {
	    $(this).removeClass('open');
	}
	$toggleBox.slideToggle();
	return false;
    });

    // Slide Fabric swatches open/close
    $('a#fabric-open').click(function() {
	$fabricBox = $(this).parent().siblings('.fabric-hidden');

	if(($fabricBox).is(':hidden')) {
	    $(this).removeClass('is-closed');
	    $(this).addClass('is-open');
	} else {
	    $(this).removeClass('is-open');
	    $(this).addClass('is-closed');
	}
	$fabricBox.slideToggle();
	return false;
    });

    // On fabric swatch click do stuff
    $('.fabric-swatch').click(function() {
	var $fabric_name = $(this).find('h3').html();

	$('.fabric-swatch').removeClass('fabric-selected');
	$(this).addClass('fabric-selected');
	$('#enquiry-open').css("display","none");
	$('#fabric-type').html($fabric_name);
	$('#fabric-choice').attr("value", $fabric_name);
    });

    //Image gallery switch out
    $("#image-gallery li img").click(function(){
	$("#image-gallery li img").removeClass('curr-img');
	$('#main-image').attr('src',$(this).attr('src').replace('http://www.prestburyupholstery.com/wp-content/themes/creative-prestbury/tt.php?w=50&h=50&src=', ''));
	    $(this).addClass('curr-img');
    });

    var imgSwap = [];

    $(".gallery li img").each(function(){
	imgUrl = this.src;
	imgSwap.push(imgUrl);
    });

    // Show sub nav's sub-subnav if available
    var current_tax = $('#taxonomy').html();

    if(current_tax) {
	/*
	if($('.subnav .cats li a').is(':contains(' + current_tax + ')')){
	console.log($(this));
	$(this).parent().addClass('current-cat');

	}
	*/
	$('.subnav .cats li a').filter(function() {
	    return $(this).text() == current_tax;
	}).parent().addClass('current-cat');
    }

    $('.subnav li.current-cat').find('.children').css('display','block');
    $('.subnav .children li.current-cat').parent('.children').css('display','block');

    // Select nth elements and apply effects
    $('#image-gallery li img:first').addClass('curr-img');
    $('article .product-thumb:nth-of-type(4n+1)').css('margin-left','0');
    $('.subnav .cats .children li:last-child').css('padding','10px 0 0');
});
