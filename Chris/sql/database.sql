create database if not exists blog;
use blog;
create TABLE if not exists blogPost (postId SMALLINT, titleOfPost VARCHAR(40), messageOfPost TEXT, timeStamp TIMESTAMP, whoPosted VARCHAR(15), lastEdit DATETIME, postViewable BIT(1), PRIMARY KEY (postId));  
create TABLE if not exists blogComment (commentId SMALLINT, whoPosted VARCHAR(15), messageOfComment TEXT, timeStamp TIMESTAMP, PRIMARY KEY (commentId));
create TABLE if not exists users (username VARCHAR(15), userEmail VARCHAR(64), userRealName VARCHAR(64), userGender BIT(1), userAge TINYINT, password VARCHAR(512), passwordSalt VARCHAR(36), PRIMARY KEY (username));
Create TABLE if not exists categories (categoryId SMALLINT, categoriesName VARCHAR(30), PRIMARY KEY (categoryId));
create TABLE if not exists blogpostCategory (postId SMALLINT, categoryId SMALLINT, PRIMARY KEY (postId, categoryId));
